import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ResultModule } from './quiz/result/result.module';
import { QuizModule } from './quiz/quiz/quiz.module';
import { QuestionModule } from './quiz/question/question.module';
import config from './config/keys';


@Module({
  imports: [MongooseModule.forRoot(config.mongoURI), QuizModule,ResultModule,QuestionModule]
})
export class AppModule {}
