import {
    Controller,
    Get,
    Post,
    Put,
    Delete,
    Body,
    Param,
  } from '@nestjs/common';
  import { CreateQuestionDto } from '../dto/create-question.dto';
  import { Question } from '../interface/question.interface';
  import { QuestionService } from '../service/question.service';

@Controller('question')
export class QuestionController {
    constructor(private readonly questionService: QuestionService) {}

    @Get(':quizId')
    findAll(@Param('quizId') quizId: string): Promise<Question[]> {
      return this.questionService.findAll(quizId);
    }
  
    @Get(':id')
    findOne(@Param('id') id: string): Promise<Question> {
      return this.questionService.findOne(id);
    }
  
    @Post()
    create(@Body() createquestionDto: CreateQuestionDto): Promise<Question> {
      return this.questionService.create(createquestionDto);
    }
  
    @Delete(':id')
    delete(@Param('id') id: string): Promise<Question> {
      return this.questionService.delete(id);
    }
  
    @Put(':id')
    update(
      @Param('id') id: string,
      @Body() updatequestionDto: CreateQuestionDto,
    ): Promise<Question> {
      return this.questionService.update(id, updatequestionDto);
    }
}
