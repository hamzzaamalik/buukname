export class CreateQuestionDto {
    readonly question: string;
    readonly optionA: string;
    readonly optionB: string;
    readonly optionC: string;
    readonly optionD: string;
    readonly correctAns: string;
    readonly quizId: string;
}
  