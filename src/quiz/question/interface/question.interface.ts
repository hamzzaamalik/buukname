export interface Question {
    id?: string;
    question: string;
    optionA: string;
    optionB: string;
    optionC: string;
    optionD: string;
    correctAns : string;
    quizId : string;
  }
  