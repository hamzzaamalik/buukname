import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { QuestionController } from './controller/question.controller';
import { QuestionService } from './service/question.service';
import { QuestionClass, QuestionSchema } from './schema/question.schema';

@Module({
    imports: [
      MongooseModule.forFeature([
        { name: QuestionClass.name, schema: QuestionSchema },
      ]),
    ],
    controllers: [QuestionController],
    providers: [QuestionService],
  })
export class QuestionModule {}
