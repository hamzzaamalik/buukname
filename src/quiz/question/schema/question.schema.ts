import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema()
export class QuestionClass extends Document {
  @Prop()
  question: string;

  @Prop()
  optionA: string;

  @Prop()
  optionB: string;
  
  @Prop()
  optionC: string;
  
  @Prop()
  optionD: string;

  @Prop()
  correctAns: string;
  
  @Prop()
  quizId: string;

}

export const QuestionSchema = SchemaFactory.createForClass(QuestionClass);
