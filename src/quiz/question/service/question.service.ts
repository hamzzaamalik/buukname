import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Question } from '../interface/question.interface';
import { QuestionClass } from '../schema/question.schema';

@Injectable()
export class QuestionService {
    constructor(
        @InjectModel(QuestionClass.name)
        private questionModel: Model<QuestionClass>,
      ) {}
    
    
      async findAll(quizId: string): Promise<Question[]> {
        return await this.questionModel.find({ quizId : quizId }).exec();;
      }

      async findOne(id: string): Promise<Question> {
        return await this.questionModel.findOne({ _id: id }).exec();
      }
    
      async create(question: Question): Promise<Question> {
        const newQuestion = new this.questionModel(question);
        return await newQuestion.save();
      }
    
      async delete(id: string): Promise<Question> {
        return await this.questionModel.findByIdAndRemove(id).exec();
      }
    
      async update(id: string, question: Question): Promise<Question> {
        return await this.questionModel.findByIdAndUpdate(id, question, {
          new: true,
        }).exec();
      }
}
