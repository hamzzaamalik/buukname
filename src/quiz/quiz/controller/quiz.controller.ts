import {
    Controller,
    Get,
    Post,
    Put,
    Delete,
    Body,
    Param,
  } from '@nestjs/common';
  import { CreateQuizDto } from '../dto/create-quiz.dto';
  import { Quiz } from '../interface/quiz.interface';
  import { QuizService } from '../service/quiz.service';

@Controller('quiz')
export class QuizController {
    constructor(private readonly quizService: QuizService) {}
  
    @Get()
    findAll(): Promise<Quiz[]> {
      return this.quizService.findAll();
    }
  
    @Get(':id')
    findOne(@Param('id') id: string): Promise<Quiz> {
      return this.quizService.findOne(id);
    }
  
    @Post()
    create(@Body() createquizDto: CreateQuizDto): Promise<Quiz> {
      return this.quizService.create(createquizDto);
    }
  
    @Delete(':id')
    delete(@Param('id') id: string): Promise<Quiz> {
      return this.quizService.delete(id);
    }
  
    @Put(':id')
    update(
      @Param('id') id: string,
      @Body() updatequizDto: CreateQuizDto,
    ): Promise<Quiz> {
      return this.quizService.update(id, updatequizDto);
    }
}
