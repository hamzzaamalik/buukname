export class CreateQuizDto {
    readonly name: string;
    readonly description: string;
    readonly endDate: string;
    readonly duration: string;
}