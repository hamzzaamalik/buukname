export interface Quiz {
    id?: string;
    name: string;
    endDate: string;
    description: string;
    duration: string;
  }
  