import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { QuizController } from './controller/quiz.controller';
import { QuizService } from './service/quiz.service';
import { QuizClass, QuizSchema } from './schema/quiz.schema';

@Module({
    imports: [
      MongooseModule.forFeature([
        { name: QuizClass.name, schema: QuizSchema },
      ]),
    ],
    controllers: [QuizController],
    providers: [QuizService],
  })
  export class QuizModule {}
