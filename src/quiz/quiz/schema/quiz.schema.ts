import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema()
export class QuizClass extends Document {
  @Prop()
  name: string;

  @Prop()
  endDate: string;

  @Prop()
  description: string;

  @Prop()
  duration: string;
}

export const QuizSchema = SchemaFactory.createForClass(QuizClass);
