import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Quiz } from '../interface/quiz.interface';
import { QuizClass } from '../schema/quiz.schema';

@Injectable()
export class QuizService {
    constructor(
        @InjectModel(QuizClass.name)
        private quizModel: Model<QuizClass>,
      ) {}
    
      async findAll(): Promise<Quiz[]> {
        return await this.quizModel.find().exec();
      }
    
      async findOne(id: string): Promise<Quiz> {
        return await this.quizModel.findOne({ _id: id });
      }
    
      async create(quiz: Quiz): Promise<Quiz> {
        const newQuiz = new this.quizModel(quiz);
        return await newQuiz.save();
      }
    
      async delete(id: string): Promise<Quiz> {
        return await this.quizModel.findByIdAndRemove(id);
      }
    
      async update(id: string, quiz: Quiz): Promise<Quiz> {
        return await this.quizModel.findByIdAndUpdate(id, quiz, {
          new: true,
        });
      }
}
