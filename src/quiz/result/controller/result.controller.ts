import {
    Controller,
    Get,
    Post,
    Put,
    Delete,
    Body,
    Param,
  } from '@nestjs/common';
  import { CreateResultDto } from '../dto/create-result.dto';
  import { Result } from '../interface/result.interface';
  import { ResultService } from '../services/result.service';
  
  @Controller('result')
export class ResultController {
    constructor(private readonly resultService: ResultService) {}

  @Get()
  findAll(): Promise<Result[]> {
    return this.resultService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string): Promise<Result> {
    return this.resultService.findOne(id);
  }

  @Post()
  create(@Body() createresultDto2: CreateResultDto): Promise<Result> {
    return this.resultService.create(createresultDto2);
  }

  @Delete(':id')
  delete(@Param('id') id: string): Promise<Result> {
    return this.resultService.delete(id);
  }

  @Put(':id')
  update(
    @Param('id') id: string,
    @Body() updateResultDto: CreateResultDto,
  ): Promise<Result> {
    return this.resultService.update(id, updateResultDto);
  }
}
