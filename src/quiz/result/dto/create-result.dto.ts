export class CreateResultDto {
  readonly correct: string;
  readonly wrong: string;
  readonly duration: string;
  readonly createdAt : string;
}