export interface Result {
  id?: string;
  correct: string;
  wrong: string;
  duration: string;
  createdAt : string;
}
