import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ResultController } from './controller/result.controller';
import { ResultService } from './services/result.service';
import { ResultClass, ResultSchema } from './schema/result.schema';

@Module({
    imports: [
      MongooseModule.forFeature([
        { name: ResultClass.name, schema: ResultSchema },
      ]),
    ],
    controllers: [ResultController],
    providers: [ResultService],
  })
export class ResultModule {}
