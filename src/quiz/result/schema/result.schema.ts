import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema()
export class ResultClass extends Document {
  @Prop()
  correct: string;

  @Prop()
  wrong: string;

  @Prop()
  duration: string;

  @Prop()
  createdAt: string;
}

export const ResultSchema = SchemaFactory.createForClass(ResultClass);
