import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Result } from '../interface/result.interface';
import { ResultClass } from '../schema/result.schema';

@Injectable()
export class ResultService {
    constructor(
        @InjectModel(ResultClass.name)
        private resultModel: Model<ResultClass>,
      ) {}
    
      async findAll(): Promise<Result[]> {
        return await this.resultModel.find().exec();
      }
    
      async findOne(id: string): Promise<Result> {
        return await this.resultModel.findOne({ _id: id });
      }
    
      async create(result: Result): Promise<Result> {
        const newResult = new this.resultModel(result);
        return await newResult.save();
      }
    
      async delete(id: string): Promise<Result> {
        return await this.resultModel.findByIdAndRemove(id);
      }
    
      async update(id: string, result: Result): Promise<Result> {
        return await this.resultModel.findByIdAndUpdate(id, result, {
          new: true,
        });
      }
}
